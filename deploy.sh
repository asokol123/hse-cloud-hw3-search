#!/bin/bash
set -Eeuo pipefail

cd ./deploy/terraform
terraform apply -auto-approve
output=`terraform output -json`
cd ../..
secret_key=`echo "$output" | jq -r .object_storage_key_secret.value`
access_key=`echo "$output" | jq -r .object_storage_key_access.value`
sed "s/{{ ACCESS_KEY }}/${access_key}/" ./deploy/secret.yaml.template > ./deploy/secret.yaml
sed -i "s/{{ SECRET_KEY }}/${secret_key}/" ./deploy/secret.yaml

yc managed-kubernetes cluster get-credentials hse-cloud-k8s-cluster --external --force

kubectl apply -k .

CLUSTER_ID=$(yc managed-kubernetes cluster get hse-cloud-k8s-cluster| grep '^id: '| awk '{print $2}')
echo "Cluster id: $CLUSTER_ID"

yc managed-kubernetes cluster get --id $CLUSTER_ID --format json | \
    jq -r .master.master_auth.cluster_ca_certificate | \
    awk '{gsub(/\\n/,"\n")}1' > ca.pem
echo "ca.pem downloaded"

SA_TOKEN=$(kubectl -n kube-system get secret $(kubectl -n kube-system get secret | \
    grep gitlab-admin | \
    awk '{print $1}') -o json | \
    jq -r .data.token | \
    base64 --d)
echo "Got sa token"

MASTER_ENDPOINT=$(yc managed-kubernetes cluster get --id $CLUSTER_ID \
    --format json | \
    jq -r .master.endpoints.external_v4_endpoint)
echo "Master endpoint: $MASTER_ENDPOINT"

kubectl config set-cluster hse-cloud-k8s-cluster \
               --certificate-authority=ca.pem \
               --server=$MASTER_ENDPOINT \
               --kubeconfig=kubeconfig
kubectl config set-credentials gitlab-admin \
                --token=$SA_TOKEN \
                --kubeconfig=kubeconfig
kubectl config set-context default \
               --cluster=hse-cloud-k8s-cluster \
               --user=gitlab-admin \
               --kubeconfig=kubeconfig
kubectl config use-context default \
               --kubeconfig=kubeconfig
