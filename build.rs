fn main() -> std::io::Result<()> {
    println!("cargo:rerun-if-changed=./proto/search.proto,./proto/types.proto,./proto/user.proto");
    tonic_build::configure()
        .type_attribute(".", "#[derive(::serde::Deserialize)]")
        .compile(
            &["./proto/search.proto", "./proto/user.proto"],
            &["./proto"],
        )
}
