FROM ekidd/rust-musl-builder:1.57.0 as builder

COPY ./Cargo.lock ./Cargo.toml ./
RUN mkdir -p src/bin && \
    echo 'fn main(){}' > src/bin/meta_search.rs && \
    echo 'fn main(){}' > src/bin/shards_search.rs && \
    echo 'fn main(){}' > src/bin/simple_search.rs && \
    echo 'fn main(){}' > src/bin/user_service.rs
RUN cargo build --release
COPY ./src ./src
COPY ./proto ./proto
COPY ./build.rs ./build.rs
RUN cargo build --release

RUN curl -L https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/v0.4.7/grpc_health_probe-linux-amd64 -o grpc_health_probe \
    && chmod +x grpc_health_probe

FROM alpine:latest as user_service
WORKDIR /app
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/user_service ./
COPY --from=builder /home/rust/src/grpc_health_probe ./
CMD ["/app/user_service"]

FROM alpine:latest as meta_search
WORKDIR /app
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/meta_search ./
COPY --from=builder /home/rust/src/grpc_health_probe ./
CMD ["/app/meta_search"]

FROM alpine:latest as shards_search
WORKDIR /app
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/shards_search ./
COPY --from=builder /home/rust/src/grpc_health_probe ./
CMD ["/app/shards_search"]

FROM alpine:latest as simple_search
WORKDIR /app
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/simple_search ./
COPY --from=builder /home/rust/src/grpc_health_probe ./
CMD ["/app/simple_search"]
