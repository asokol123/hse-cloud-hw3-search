#!/usr/bin/env python3

import sys
import subprocess


SIMPLE_SEARCH = 'simple-search'
ALLOWED_SERVICES = [SIMPLE_SEARCH, 'meta-search', 'shards-search', 'user-service']
CR = 'cr.yandex/crpmkp475nqv49gp6tfu'


def service_from_tag(tag: str) -> str:
    idx = tag.index('-')
    return tag[idx+1:]


def image_from_service(service: str) -> str:
    return service.replace('-', '_')


def update_image(service_name: str, image_name: str):
    subprocess.check_output(['kubectl', 'set', 'image', f'deployment/{service_name}', f'{service_name}={CR}/{image_name}'])


if __name__ == '__main__':
    tag_name = sys.argv[1]
    service = service_from_tag(tag_name)
    if service not in ALLOWED_SERVICES:
        raise ValueError(f'Invalid service {service}')
    image = image_from_service(service)
    image = f'{image}:{tag_name}'
    if service == SIMPLE_SEARCH:
        update_image(f'{service}-1', image)
        update_image(f'{service}-2', image)
    else:
        update_image(service, image)
