use crate::{
    rpc::search::{search_server::Search, SearchRequest, SearchResponse},
    simple_search::{Client, Server as SServer},
};
use futures::future::try_join_all;
pub use tonic::transport::Uri;
use tonic::{codegen::http::uri::InvalidUri, Request, Response, Status};

pub struct Server {
    clients: Vec<Client>,
}

impl Server {
    pub async fn try_new(addrs: impl Iterator<Item = &str>) -> Result<Self, InvalidUri> {
        Ok(Self {
            clients: addrs
                .map(|addr| -> Result<Client, InvalidUri> { Ok(Client::new(addr.parse()?)) })
                .collect::<Result<Vec<_>, _>>()?,
        })
    }
}

#[tonic::async_trait]
impl Search for Server {
    async fn search(
        &self,
        req: Request<SearchRequest>,
    ) -> Result<Response<SearchResponse>, Status> {
        let req = req.into_inner();
        log::info!(
            "Shard search starting requests, clients: len {:?}",
            self.clients.len()
        );
        let results = try_join_all(self.clients.iter().map(|c| c.search(req.clone())))
            .await?
            .into_iter()
            .flat_map(|r| r.results)
            .collect();

        let last_filter = SServer::new(results);

        Ok(Response::new(SearchResponse {
            results: last_filter.get_search_data(&req.text, req.user, req.limit),
        }))
    }
}
