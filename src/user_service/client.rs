pub use crate::rpc::types::User;
use crate::rpc::user::{user_service_client::UserServiceClient, GetUserRequest};
pub use tonic::transport::{Error, Uri};
pub use tonic::Status;

#[derive(Clone)]
pub struct Client {
    inner: UserServiceClient<tonic::transport::Channel>,
}

impl Client {
    pub fn new(addr: Uri) -> Self {
        Self {
            inner: UserServiceClient::new(tonic::transport::Endpoint::from(addr).connect_lazy()),
        }
    }

    pub async fn get_user(&self, user_id: String) -> Result<User, Status> {
        self.inner
            .clone()
            .get_user(GetUserRequest { user_id })
            .await
            .map(|r| r.into_inner())
    }
}
