use std::collections::HashMap;
use tonic::{Request, Response, Status};

use crate::common::DataSource;
use crate::rpc::{
    types::User as ProtoUser,
    user::{user_service_server::UserService, GetUserRequest},
};

#[derive(serde::Deserialize)]
pub struct User {
    user_id: String,
    gender: String,
    age: i32,
}

pub struct Server {
    data: HashMap<String, User>,
}

impl Server {
    pub fn try_new<T>(data_reader: T) -> Result<Self, T::Error>
    where
        T: DataSource<Output = User>,
    {
        Ok(Self {
            data: data_reader
                .read_data()?
                .into_iter()
                .map(|user| (user.user_id.clone(), user))
                .collect(),
        })
    }
}

#[tonic::async_trait]
impl UserService for Server {
    async fn get_user(&self, req: Request<GetUserRequest>) -> Result<Response<ProtoUser>, Status> {
        let id = req.into_inner().user_id;
        let user = self
            .data
            .get(&id)
            .ok_or_else(|| Status::not_found(format!("User with id {} not found", id)))?;
        Ok(Response::new(ProtoUser {
            gender: user.gender.clone(),
            age: user.age,
        }))
    }
}
