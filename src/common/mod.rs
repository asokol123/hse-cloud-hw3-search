mod csv;
pub use self::csv::CsvDataSource;

pub trait DataSource {
    type Output;
    type Error;

    fn read_data(&self) -> Result<Vec<Self::Output>, Self::Error>;
}
