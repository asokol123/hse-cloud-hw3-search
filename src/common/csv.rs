use std::path::Path;

use super::DataSource;

pub struct CsvDataSource<T: serde::de::DeserializeOwned, P: AsRef<Path>> {
    path: P,
    _phantomdata: std::marker::PhantomData<fn() -> T>,
}

impl<T: serde::de::DeserializeOwned, P: AsRef<Path>> CsvDataSource<T, P> {
    pub fn new(path: P) -> Self {
        Self {
            path,
            _phantomdata: Default::default(),
        }
    }
}

impl<T: serde::de::DeserializeOwned, P: AsRef<Path>> DataSource for CsvDataSource<T, P> {
    type Output = T;
    type Error = csv::Error;

    fn read_data(&self) -> Result<Vec<Self::Output>, Self::Error> {
        let reader = csv::Reader::from_path(self.path.as_ref())?;
        let mut result = Vec::new();
        for record in reader.into_deserialize() {
            result.push(record?);
        }
        Ok(result)
    }
}
