pub mod types {
    tonic::include_proto!("types");
}

pub mod search {
    tonic::include_proto!("search");
}

pub mod user {
    tonic::include_proto!("user_service");
}
