use std::env;

use search::common::CsvDataSource;
use search::simple_search::Server;
use search::rpc::search::search_server::SearchServer;
use tonic_health::ServingStatus;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    env_logger::init();
    let data_file = env::var("DATA_FILE").expect("DATA_FILE env is required");
    let bind_addr = env::var("BIND_ADDR")
        .unwrap_or_else(|_| "127.0.0.1:5005".to_owned())
        .parse()
        .expect("Failed to parse address");

    let (mut health_reporter, health_service) = tonic_health::server::health_reporter();
    health_reporter.set_service_status("", ServingStatus::Serving).await;

    let data_loader = CsvDataSource::new(&data_file);
    let server = Server::try_new(data_loader).expect("Failed to create server");
    let server = SearchServer::new(server);
    tonic::transport::Server::builder()
        .add_service(health_service)
        .add_service(server)
        .serve(bind_addr)
        .await
        .expect("Failed to serve");
}
