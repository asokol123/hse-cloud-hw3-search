use std::{collections::HashMap, env};

use actix_web::{
    error, get,
    http::StatusCode,
    web::{self, Json},
    App, HttpServer,
};
use search::{simple_search::Client as SClient, user_service::Client as UClient};
use serde::Deserialize;

#[derive(Clone)]
struct Server {
    user: UClient,
    search: SClient,
}

#[derive(Debug, Deserialize)]
struct Query {
    text: String,
    user_id: Option<String>,
    limit: Option<u32>,
}

#[get("/check")]
async fn check_handler() -> &'static str {
    "Check ok, final version"
}

#[get("/search")]
async fn search_handler(
    query: web::Query<Query>,
    data: web::Data<Server>,
) -> actix_web::Result<Json<Vec<HashMap<String, String>>>> {
    let query = query.into_inner();
    let user = if let Some(user_id) = query.user_id {
        match data.user.get_user(user_id).await {
            Ok(resp) => Some(resp),
            Err(err) => {
                if err.code() == tonic::Code::NotFound {
                    None
                } else {
                    return Err(
                        error::InternalError::new(err, StatusCode::INTERNAL_SERVER_ERROR).into(),
                    );
                }
            }
        }
    } else {
        None
    };
    log::info!(
        "Sending search query from meta with text: {}, user: {:?}, limit: {:?}",
        &query.text,
        &user,
        &query.limit
    );
    let search_data = data
        .search
        .search_3(query.text, user, query.limit)
        .await
        .map_err(|e| error::InternalError::new(e, StatusCode::INTERNAL_SERVER_ERROR))?
        .results
        .into_iter()
        .map(|x| {
            HashMap::from([
                ("document".to_owned(), x.document),
                ("key".to_owned(), x.key),
                ("key_md5".to_owned(), x.key_md5),
            ])
        })
        .collect();
    Ok(Json(search_data))
}

#[actix_web::main]
async fn main() {
    env_logger::init();

    let user_addr = env::var("USER_SERVICE")
        .expect("USER_SERVICE env is required")
        .parse()
        .expect("Invalid use address");
    let shard_addr = env::var("SHARD_SEARCH_SERVICE")
        .expect("SHARD_SEARCH_SERVICE env is required")
        .parse()
        .expect("Invalid shard search address");
    let bind_addr = env::var("BIND_ADDR").unwrap_or_else(|_| "127.0.0.1:8000".to_owned());

    let server = Server {
        user: UClient::new(user_addr),
        search: SClient::new(shard_addr),
    };

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(server.clone()))
            .service(search_handler)
            .service(check_handler)
    })
    .bind(bind_addr)
    .expect("Failed to bind")
    .run()
    .await
    .expect("Actix failed");
}
