use crate::rpc::search::SearchRequest;

pub use crate::rpc::{
    search::{search_client::SearchClient, SearchResponse},
    types::User,
};
pub use tonic::{
    transport::{Error, Uri},
    Status,
};

#[derive(Clone)]
pub struct Client {
    inner: SearchClient<tonic::transport::Channel>,
}

impl Client {
    pub fn new(addr: Uri) -> Self {
        Self {
            inner: SearchClient::new(tonic::transport::Endpoint::from(addr).connect_lazy()),
        }
    }

    pub async fn search(&self, req: SearchRequest) -> Result<SearchResponse, Status> {
        self.inner.clone().search(req).await.map(|r| r.into_inner())
    }

    pub async fn search_3(
        &self,
        text: String,
        user: Option<User>,
        limit: Option<u32>,
    ) -> Result<SearchResponse, Status> {
        self.search(SearchRequest { text, user, limit }).await
    }
}
