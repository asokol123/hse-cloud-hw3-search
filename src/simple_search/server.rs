use crate::{
    common::DataSource,
    rpc::{
        search::{search_server::Search, SearchRequest, SearchResponse, SearchResult},
        types::User,
    },
};
use tonic::{Request, Response, Status};

pub type News = SearchResult;

pub struct Server {
    news: Vec<News>,
}

impl Server {
    pub fn try_new<T>(data_reader: T) -> Result<Self, T::Error>
    where
        T: DataSource<Output = News>,
    {
        Ok(Self {
            news: data_reader.read_data()?,
        })
    }

    pub fn new(data: Vec<News>) -> Self {
        Self { news: data }
    }

    fn stupid_count_tokens(tokens: &[&str], text: &str) -> u64 {
        let mut result = 0;
        for token in tokens {
            // yes, this matches male in female. 11/10
            if text.contains(token) {
                result += 1;
            }
        }
        result
    }

    fn build_tokens_count(&self, search_text: &str) -> Vec<u64> {
        let tokens: Vec<_> = search_text.split_whitespace().collect();
        self.news
            .iter()
            .map(|n| Self::stupid_count_tokens(&tokens, &n.document))
            .collect()
    }

    fn get_gender_mask(&self, user_data: Option<User>) -> Vec<u64> {
        let ud = match user_data {
            Some(user) => user.gender,
            None => "non-existing gender".to_owned(),
        };
        self.news
            .iter()
            .map(|n| Self::stupid_count_tokens(&[&ud], &n.gender))
            .collect()
    }

    fn get_age_mask(&self, user_data: Option<User>) -> Vec<bool> {
        let age = match user_data {
            Some(user) => user.age,
            None => -1,
        };
        self.news
            .iter()
            .map(|n| n.age_from <= age && age <= n.age_to)
            .collect()
    }

    fn sort_by_rating_and_tokens(
        &self,
        gender_mask: &[u64],
        age_mask: &[bool],
        tokens_count: &[u64],
    ) -> Vec<usize> {
        let mut indices: Vec<_> = (0..self.news.len()).collect();
        indices.sort_unstable_by_key(|&i| {
            std::cmp::Reverse((
                tokens_count[i],
                gender_mask[i] + age_mask[i] as u64,
                &self.news[i].key_md5,
            ))
        });
        indices
    }

    pub fn get_search_data(
        &self,
        search_text: &str,
        user_data: Option<User>,
        limit: Option<u32>,
    ) -> Vec<News> {
        if search_text.is_empty() {
            return Vec::new();
        }
        let limit = limit.unwrap_or(10);
        let tokens_count = self.build_tokens_count(search_text);
        let gender_mask = self.get_gender_mask(user_data.clone());
        let age_mask = self.get_age_mask(user_data);
        let sorted = self.sort_by_rating_and_tokens(&gender_mask, &age_mask, &tokens_count);
        sorted
            .into_iter()
            .take(limit as usize)
            .map(|i| self.news[i].clone())
            .collect()
    }
}

#[tonic::async_trait]
impl Search for Server {
    async fn search(
        &self,
        req: Request<SearchRequest>,
    ) -> Result<Response<SearchResponse>, Status> {
        let req = req.into_inner();
        Ok(Response::new(SearchResponse {
            results: self.get_search_data(&req.text, req.user, req.limit),
        }))
    }
}
