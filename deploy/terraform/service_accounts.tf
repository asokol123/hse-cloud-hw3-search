resource "yandex_iam_service_account" "kubernetes_cluster" {
  folder_id = local.folder_id
  name      = "hse-cloud-kubernetes-service-account"
}

resource "yandex_resourcemanager_folder_iam_member" "kubernetes_cluster_editor" {
  folder_id = local.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.kubernetes_cluster.id}"
}

resource "yandex_iam_service_account" "kubernetes_node" {
  folder_id = local.folder_id
  name      = "hse-cloud-kubernetes-node-service-account"
}

resource "yandex_iam_service_account" "storage_admin" {
  folder_id = local.folder_id
  name      = "hse-cloud-storage-admin"
}

resource "yandex_resourcemanager_folder_iam_member" "storage_admin_editor" {
  folder_id = local.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.storage_admin.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "kubernetes_node_puller" {
  folder_id = local.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.kubernetes_node.id}"
}

resource "yandex_iam_service_account_static_access_key" "storage_admin" {
  service_account_id = yandex_iam_service_account.storage_admin.id
  description        = "static access key for storage admin"
}