output "object_storage_key_access" {
  value = yandex_iam_service_account_static_access_key.storage_admin.access_key
}

output "object_storage_key_secret" {
  value     = yandex_iam_service_account_static_access_key.storage_admin.secret_key
  sensitive = true
}
