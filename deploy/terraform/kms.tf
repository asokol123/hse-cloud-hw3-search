resource "yandex_kms_symmetric_key" "kms_key" {
  name              = "hse-cloud-kms-key"
  description       = "HSE cloud kms key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" // equal to 1 year
}