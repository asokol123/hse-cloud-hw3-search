resource "yandex_kubernetes_cluster" "hse_cloud_k8s_cluster" {
  name        = "hse-cloud-k8s-cluster"
  description = "HSE cloud cluster"

  network_id = yandex_vpc_network.network.id

  master {
    version = "1.20"
    zonal {
      zone      = yandex_vpc_subnet.subnet.zone
      subnet_id = yandex_vpc_subnet.subnet.id
    }

    public_ip = true
  }

  service_account_id      = yandex_iam_service_account.kubernetes_cluster.id
  node_service_account_id = yandex_iam_service_account.kubernetes_node.id

  release_channel = "STABLE"

  kms_provider {
    key_id = yandex_kms_symmetric_key.kms_key.id
  }

  depends_on = [
    yandex_resourcemanager_folder_iam_member.storage_admin_editor,
    yandex_resourcemanager_folder_iam_member.kubernetes_node_puller
  ]
}

resource "yandex_kubernetes_node_group" "hse_k8s_node_group" {
  cluster_id  = yandex_kubernetes_cluster.hse_cloud_k8s_cluster.id
  name        = "hse-cloud-k8s-node-group"
  description = "HSE cloud cluster node group"
  version     = "1.20"

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat        = true
      subnet_ids = ["${yandex_vpc_subnet.subnet.id}"]
    }

    resources {
      memory = 2
      cores  = 2
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = true
    }
  }

  scale_policy {
    auto_scale {
      min     = 5
      max     = 7
      initial = 7
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-c"
    }
  }
}