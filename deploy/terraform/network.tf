resource "yandex_vpc_network" "network" {
  name = "hse-cloud-network"
}

resource "yandex_vpc_subnet" "subnet" {
  name           = "hse-cloud-subnet"
  zone           = "ru-central1-c"
  network_id     = yandex_vpc_network.network.id
  v4_cidr_blocks = ["10.128.0.0/24"]
}