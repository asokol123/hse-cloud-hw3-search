resource "yandex_storage_bucket" "data" {
  access_key = yandex_iam_service_account_static_access_key.storage_admin.access_key
  secret_key = yandex_iam_service_account_static_access_key.storage_admin.secret_key
  bucket     = "hse-cloud-data"
}

resource "yandex_storage_object" "news_1" {
  bucket     = yandex_storage_bucket.data.bucket
  key        = "news-1.csv"
  source     = "../../data/news_generated.1.csv"
  access_key = yandex_iam_service_account_static_access_key.storage_admin.access_key
  secret_key = yandex_iam_service_account_static_access_key.storage_admin.secret_key
}

resource "yandex_storage_object" "news_2" {
  bucket     = yandex_storage_bucket.data.bucket
  key        = "news-2.csv"
  source     = "../../data/news_generated.2.csv"
  access_key = yandex_iam_service_account_static_access_key.storage_admin.access_key
  secret_key = yandex_iam_service_account_static_access_key.storage_admin.secret_key
}

resource "yandex_storage_object" "users" {
  bucket     = yandex_storage_bucket.data.bucket
  key        = "users.csv"
  source     = "../../data/users.csv"
  access_key = yandex_iam_service_account_static_access_key.storage_admin.access_key
  secret_key = yandex_iam_service_account_static_access_key.storage_admin.secret_key
}